FROM registry.git.beagleboard.org/jkridner/debian-build:bookworm as sphinx-build-env
ARG TARGETARCH
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y update \
    && apt-get install -y locales \
	&& localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && apt-get clean
ENV LANG en_US.utf8
RUN apt-get -y update \
    && apt-get install -y \
        make rsync git wget \
        doxygen graphviz \
        librsvg2-bin texlive-latex-base texlive-latex-extra latexmk texlive-fonts-recommended \
        python3 python3-pip python3-pil \
        imagemagick-6.q16 librsvg2-bin webp \
        texlive-full texlive-latex-extra texlive-fonts-extra \
        fonts-freefont-otf fonts-dejavu fonts-dejavu-extra fonts-freefont-ttf \
        python3-venv \
        nginx \
        bb-code-server \
        openssh-client \
    && apt-get clean
RUN python3 -m venv .venv \
    && . .venv/bin/activate \
    && python -m pip install --upgrade pip \
    && pip install -U \
        sphinx==7.2.6 \
        sphinx-rtd-theme \
        pydata_sphinx_theme \
        sphinx_design \
        sphinx-tabs \
        sphinxcontrib.svg2pdfconverter \
        sphinx-notfound-page \
        sphinxext-rediraffe \
        sphinx_copybutton \
        sphinxcontrib-images \
        breathe \
        exhale \
        graphviz \
        sphinxcontrib-youtube
RUN if [ "$TARGETARCH" = "arm64" ]; then \
        wget https://github.com/pdfcpu/pdfcpu/releases/download/v0.4.0/pdfcpu_0.4.0_Linux_arm64.tar.xz \
        && tar xf pdfcpu_0.4.0_Linux_arm64.tar.xz && mv pdfcpu_0.4.0_Linux_arm64/pdfcpu /usr/local/bin/ && /usr/local/bin/pdfcpu version ; \
    elif [ "$TARGETARCH" = "amd64" ]; then \
        wget https://github.com/pdfcpu/pdfcpu/releases/download/v0.4.0/pdfcpu_0.4.0_Linux_x86_64.tar.xz \
        && tar xf pdfcpu_0.4.0_Linux_x86_64.tar.xz && mv pdfcpu_0.4.0_Linux_x86_64/pdfcpu /usr/local/bin/ && /usr/local/bin/pdfcpu version ; \
    fi \
    && rm -rf pdfcpu_0.4.0_Linux_*

COPY ./scripts scripts
COPY ./nginx.conf.template nginx.conf.template

#ENTRYPOINT ["./scripts/start.sh"]
